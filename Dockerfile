FROM resin/raspberrypi3-debian

RUN apt-get update && apt-get install --force-yes build-essential git
RUN git clone git://git.drogon.net/wiringPi && cd wiringPi/ && ./build
RUN mkdir -p wpi/include wpi/lib
RUN cp wiringPi/wiringPi/libwiring* wpi/lib
RUN cp wiringPi/wiringPi/wiring*.h wpi/include/

#find / -name "*wiring*"
RUN git clone https://github.com/robotrovsky/rpi_i2c_bit_banging.git && \
	cd rpi_i2c_bit_banging/src && \
	g++ -std=c++11 -L /root/wpi/wpi -I /root/wpi/include -lwiringPi -o i2cBitBangingBus i2cBitBangingBus.cpp

g++ -std=c++11 -I /root/wpi/include -c -fPIC -o i2cBitBangingBus.o i2cBitBangingBus.cpp
g++ -shared  -L /root/wpi/lib -I /root/wpi/include -lwiringPi -Wl,-soname,libi2cBitBangingBus.so -o libi2cBitBangingBus.so  i2cBitBangingBus.o
