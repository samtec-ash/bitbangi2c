CXX = g++
CXXFLAGS = -std=c++11
LDFLAGS = -shared -lwiringPi -fPIC -L,-soname
OUT_SO=./lib_bbi2c.so

all: i2cBitBangingBus

i2cBitBangingBus: src/bb_i2c.cpp
	$(CXX) $(CXXFLAGS) -o $(OUT_SO) src/bb_i2c.cpp $(LDFLAGS)

clean:
	-rm $(OUT_SO)
