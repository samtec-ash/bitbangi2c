from ctypes import cdll
lib = cdll.LoadLibrary('./lib_bbi2c.so')
lib.setup()


class BBI2CBus(object):
    def __init__(self, pin_number_sda, pin_number_scl, sleepTimeNanos=0, delayTicks=0):
        self.obj = lib.bbI2CBus_new(pin_number_sda, pin_number_scl, sleepTimeNanos, delayTicks)

    def writeByteData(self, i2c_address, command, value):
        return lib.bbI2CBus_writeByteData(self.obj, i2c_address, command, value)

    def readByteData(self, i2c_address, command):
        return lib.bbI2CBus_readByteData(self.obj, i2c_address, command)

    def writeBlockData(self, i2c_address, command, length, values):
        return lib.bbI2CBus_writeBlockData(self.obj, i2c_address, command, length, values)

    def readBlockData(self, i2c_address, command, length, values):
        return lib.bbI2CBus_readBlockData(self.obj, i2c_address, command, length, values)


if __name__ == "__main__":
    # Pin 0 = GPIO 17
    # Pin 2 = GPIO 27
    # Pin 8 = SDA.1
    # Pin 9 = SCL.1
    i2c = BBI2CBus(0, 2)
